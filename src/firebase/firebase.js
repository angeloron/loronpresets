import firebase from "firebase/app";
import "firebase/analytics";
import "firebase/firestore";
import "firebase/functions";
import "firebase/performance";
import "firebase/auth";
import firebaseConfig from './firebaseConfig';

export const myFirebase = firebase.initializeApp(firebaseConfig);
myFirebase.firestore().settings({
    cacheSizeBytes: myFirebase.firestore.CACHE_SIZE_UNLIMITED
});
myFirebase.firestore().enablePersistence()

export const db = myFirebase.firestore();

export const functions = myFirebase.functions('europe-west1');

export const perf = myFirebase.performance();

export const auth = myFirebase.auth();