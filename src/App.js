import React, { useEffect, useState } from 'react';
import { CloudinaryContext } from 'cloudinary-react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { StateProvider } from './State';
import { db } from './firebase/firebase';
import ReactGA from 'react-ga';
import GDPR from './components/GDPR';

/* DESIGN */
import { ThemeProvider, createMuiTheme, responsiveFontSizes, } from '@material-ui/core/styles';

/* SCROLL TO TOP */
import ScrollTop from './components/ScrollTop';

/* ROUTES */
import Howtoinstallpresets from './components/Howtoinstallpresets';
import LightroomPresets from './components/LightroomPresets';
import Contact from './components/Contact';
import FAQ from './components/FAQ';
import Footer from './components/Footer';
import Menu from './components/Menu';
import Cart from './components/Cart';
import Thankyou from './components/Thankyou';
import Blog from './components/Blog';
import Productpage from './components/Productpage';
import About from "./components/About";
import CookiePolicy from "./components/CookiePolicy";
import PrivacyPolicy from "./components/PrivacyPolicy";
import TermsConditions from "./components/TermsConditions";
import SalesRefunds from "./components/SalesRefunds";
import PageNotFound from "./components/PageNotFound";
import { setCookie, getCookie } from "./Functions";
import './App.css';

const App = () => {

  ReactGA.initialize('UA-156916685-2');
  ReactGA.pageview(window.location.pathname + window.location.search);

  const [items, setItems] = useState([]);
  const [bottomPopup, setBottomPopup] = useState(false);

  useEffect(() => {
    db.collection('items').orderBy("type").orderBy("id").get().then((e) => {
      let newItems = []
      e.docs.forEach(doc => {
        newItems.push(doc.data())
      });
      setItems(newItems);
    });
    getCookie("CookieLoronpresets") === null && setBottomPopup(true);
    setCookie("CookieLoronpresets", 1, 7);
  }, []);

  const initialState = {
    cart: { items: [] },
    purchases: [],
    database: [],
    GDPR: true,
  };

  const reducer = (state, action) => {

    switch (action.type) {
      case 'addItem':
        state.cart.items.push(action.item);
        return { ...state };

      case 'removeItem':
        state.cart.items = state.cart.items.filter(item => item !== action.item);
        return { ...state };

      case 'emptyCart':
        return {
          ...state,
          cart: { items: [] }
        };
      case 'addPurchase':
        return {
          ...state,
          purchases: [...state.purchases, action.purchase]
        };
      case 'GDPR':
        return {
          ...state,
          GDPR: false
        }
      default:
    };
    return state;
  };

  let theme = createMuiTheme({
    palette: {
      primary: {
        // main: '#FFFFFF',
        main: '#2E2E2E',
      },
      secondary: {
        main: '#FF0000',
        // main: '#DD6940',
      },
    },
  });

  theme = responsiveFontSizes(theme);

  return <StateProvider initialState={initialState} reducer={reducer}>
    <ThemeProvider theme={theme}>
      <CloudinaryContext cloudName="angeloron">
        <div>
          <Switch>
            <Route path='/*' render={props => <Menu {...props} />} />
          </Switch>
          <div id="back-to-top-anchor" style={{ position: 'absolute', top: 0 }} />
          <div id="mainwrapper">
            <Switch>
              <Route path='/' render={props => <LightroomPresets {...props} items={items} />} exact />
              <Route path='/LightroomPresets' render={props => <LightroomPresets {...props} items={items} />} />
              <Route path='/presets' render={props => <LightroomPresets {...props} items={items} />} />
              <Route path='/contact' component={Contact} />
              <Route path='/faq' component={FAQ} />
              <Route path='/cart' render={props => <Cart {...props} items={items} />} />
              <Route path='/thankyou' component={Thankyou} />
              <Route path='/about' component={About} />
              <Route path='/cookiepolicy' component={CookiePolicy} />
              <Route path='/privacypolicy' component={PrivacyPolicy} />
              <Route path='/termsconditions' component={TermsConditions} />
              <Route path='/salesrefunds' component={SalesRefunds} />
              <Route path='/how-to-install-lightroom-presets' component={Howtoinstallpresets} />
              <Route path='/blog' component={Blog} />
              <Route path='/product/' render={props => <Productpage {...props} items={items} />} />
              <Route path='/404' render={props => <PageNotFound {...props} items={items} />} />
              <Redirect to="/404" />
            </Switch>
          </div>
          <Footer />
          <ScrollTop />
        </div>
        {bottomPopup && <GDPR />}
      </CloudinaryContext>
    </ThemeProvider>
  </StateProvider >
}

export default App;