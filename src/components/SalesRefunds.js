import React from "react";
import CustomLink from "./CustomLink";

const SalesRefunds = ({toggleDialog}) => <div className="margin50">
    <h2><strong>Refund Policy</strong></h2>

    <p>Due to the digital nature of our products, we unfortunately do not provide refunds once they have been purchased.</p>

    <p>(Except double purchases of course)</p>

    <p>If you have any questions about your purchase, please <div className="contactmelink" onClick={() => toggleDialog(false)}><CustomLink to={'/contact'}>contact me</CustomLink></div> and I will be happy to help you</p>

</div>;

export default SalesRefunds;