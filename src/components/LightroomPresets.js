import React from 'react';
import { ButtonGroup, Button } from "@material-ui/core";
import Slider from "react-animated-slider";
import "react-animated-slider/build/horizontal.css";
import MetaTags from 'react-meta-tags';
import "./css/Slider.css";
import "./css/Presets.css";
import Displayproduct from "./Displayproduct";
import { useStateValue } from "../State";

const LightroomPresets = ({ items, history }) => {

    const [{ cart }, dispatch] = useStateValue();

    return <>
        <MetaTags>
            <meta name="description"
                content="The sole purpose of Loron Presets is to help photographers save time and still make amazing photos through state of the art lightroom presets" />
            <meta property="og:site_name" content="LORONPRESETS - Amazing lightroom Presets" />
            <meta property="og:title" content="LORONPRESETS - Amazing lightroom Presets" />
            <meta property="og:url" content="loronpresets.com" />
            <meta property="og:description"
                content="The sole purpose of Loron Presets is to help photographers save time and still make amazing photos through state of the art lightroom presets" />
            <meta property="og:image" content="//loronpresets.com/logo300x200.png" />
            <meta property="og:type" content="profile" />
            <meta itemProp="name" content="LORON LIGHTROOM PRESETS" />
            <meta itemProp="url" content="https://loronpresets.com/LightroomPresets" />
            <meta itemProp="keywords"
                content="Ange, Loron, lightroom presets, Mobile presets, Lightroom Mobile, instagram presets, Blogger presets, Presets, presets lightroom, Photography, Colour, lightroom preset, lifestyle presets, moody presets, instagram filter, preset, presets mobile, Travel presets, Mobile Lightroom, Photo filter, photo presets, Light room presets, Bright Presets, desktop presets, instagram preset, instagram blogger, filter, filter for instagram, blogger preset, lightroom, mobile preset, outdoor presets, lightroom cc, Warm presets, portrait presets, selfie preset, travel blogger, preset for lightroom, Presets & Photo Filters, presets desktop, presets free, Presets indoor, presets bright, photoshop filters, Travel lightroom, silver filter photo, summer presets, presets outdoor, presets pack, professional presets, professional wedding, rustic, wedding presets, white preset, willow presets, winter preset, trendy presets, Vibrant presets, vsco filters, vsco preset, VSCO presets, lightroom cc presets, lightroom filters, Moody Warm presets, Natural Bright Airy, outdoor natural, Mobile overlays, photographer presets, photo preset, lightroom app, light and airy, light preset, instagram vintage, instragram preset, Interior presets, iphone presets, lifestyle preset, Lightroom presets LR, love portrait photo, Lr mobile presets, Lr presets, mikutas presets, minutes preset, mobile filters, couples summer, engagements fashion, fall presets, clean desktop and clean presets, clean tone preset, Bright color, bright preset, airy presets, best brushes filters, best preset, gray presets, green white nature, indoor presets, influencer presets, inside indoors light, instagram, instagram feed, dark moody preset, dark presets, instagram filters, presets, instagram, blogger, insta presets, instagram presets, blogger presets, lightroom, adobe, photoshop, filter, photo, photography, photographer, Loron, ange, loron, freelancer, coder, web, developer, photographer, photo, freelance, code, marketing, video, design, wordpress, react, javascript, html, css, js, css3, es6, html5, about, contact, share, link, web developer, web design, web master, webmaster, responsive, website, portfolio, original" />
            <meta name="keywords"
                content="Ange, Loron, lightroom presets, Mobile presets, Lightroom Mobile, instagram presets, Blogger presets, Presets, presets lightroom, Photography, Colour, lightroom preset, lifestyle presets, moody presets, instagram filter, preset, presets mobile, Travel presets, Mobile Lightroom, Photo filter, photo presets, Light room presets, Bright Presets, desktop presets, instagram preset, instagram blogger, filter, filter for instagram, blogger preset, lightroom, mobile preset, outdoor presets, lightroom cc, Warm presets, portrait presets, selfie preset, travel blogger, preset for lightroom, Presets & Photo Filters, presets desktop, presets free, Presets indoor, presets bright, photoshop filters, Travel lightroom, silver filter photo, summer presets, presets outdoor, presets pack, professional presets, professional wedding, rustic, wedding presets, white preset, willow presets, winter preset, trendy presets, Vibrant presets, vsco filters, vsco preset, VSCO presets, lightroom cc presets, lightroom filters, Moody Warm presets, Natural Bright Airy, outdoor natural, Mobile overlays, photographer presets, photo preset, lightroom app, light and airy, light preset, instagram vintage, instragram preset, Interior presets, iphone presets, lifestyle preset, Lightroom presets LR, love portrait photo, Lr mobile presets, Lr presets, mikutas presets, minutes preset, mobile filters, couples summer, engagements fashion, fall presets, clean desktop and clean presets, clean tone preset, Bright color, bright preset, airy presets, best brushes filters, best preset, gray presets, green white nature, indoor presets, influencer presets, inside indoors light, instagram, instagram feed, dark moody preset, dark presets, instagram filters, presets, instagram, blogger, insta presets, instagram presets, blogger presets, lightroom, adobe, photoshop, filter, photo, photography, photographer, Loron, ange, loron, freelancer, coder, web, developer, photographer, photo, freelance, code, marketing, video, design, wordpress, react, javascript, html, css, js, css3, es6, html5, about, contact, share, link, web developer, web design, web master, webmaster, responsive, website, portfolio, original" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta name="twitter:title" content="LORON LIGHTROOM PRESETS" />
            <meta property="twitter:description"
                content="The sole purpose of Loron Presets is to help photographers save time and still make amazing photos through state of the art lightroom presets" />
            <meta name="twitter:url" content="https://loronpresets.com/LightroomPresets" />
            <meta name="twitter:image"
                content="https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_100,f_auto/v1581277352/loronpresets/logo.png" />
            <meta name="twitter:card" content="summary" />
            <link rel="canonical" href="https://loronpresets.com/LightroomPresets" />
            <title>Loron Presets - Ligthroom presets by professional photographers</title>
        </MetaTags>
        <Slider autoplay={5000} touchDisabled className="slider-wrapper">
            {items.map((item, index) => (
                <div
                    key={index}
                    className="slider-content"
                    style={{
                        background: item.type === 'series'
                            ?
                            `url('https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_1500,f_auto/v1581273894/loronpresets/${item.name.replace(/ /g, "_").toLowerCase()}/${item.content[0].toLowerCase().replace(/ /g, "_")}.png') no-repeat center center`
                            :
                            `url('https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_1500,f_auto/v1581273894/loronpresets/${item.name.replace(/ /g, "_").toLowerCase()}//original.png') no-repeat center center`
                    }}
                >
                    <div className="inner" itemScope itemType="http://schema.org/Offer">
                        <div className="title">
                            <h1>LORON</h1>
                            <h2>PREMIUM LIGHTROOM PRESETS</h2>
                            <div className="slogan" itemProp="name">{item.name.toUpperCase()} {item.type.toUpperCase()}</div>
                        </div>
                        <div>
                            <img src={`https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_150,f_auto/v1581273894/loronpresets/${item.name.replace(/ /g, "_").toLowerCase()}/cover.png`} alt={item.name} />
                            <div className="description" itemProp="description">
                                {item.discount ?
                                    <h1 className="red" itemProp="price"><s>{item.price}{item.currency}</s> {item.discountprice}{item.currency}</h1>
                                    :
                                    <h1 className="red" itemProp="price">ONLY {item.price}{item.currency}!</h1>
                                }
                                {item.description}
                                <ul>
                                    {item.content.map(e => <li key={e}>{e}</li>)}
                                </ul>
                            </div>
                        </div>
                        <ButtonGroup>
                            <Button variant="contained" color="primary" onClick={() => history.push(`/product/${item.name.replace(/ /g,"-").toLowerCase()}`)}>
                                READ MORE
                            </Button>
                            {cart.items.includes(item.id) ?
                                <Button
                                    color="primary"
                                    variant="contained"
                                    onClick={() => {
                                        history.push("/cart");
                                    }
                                    }
                                >
                                    CHECKOUT
                                </Button>
                                :
                                <Button
                                    color="secondary"
                                    variant="contained"
                                    onClick={() => {
                                        dispatch({
                                            type: 'addItem',
                                            item: item.id
                                        });
                                    }
                                    }
                                >
                                    ADD TO CART
                            </Button>
                            }
                        </ButtonGroup>
                    </div>
                </div>
            ))}
        </Slider>
        <div id="whyus">
            <div id="whyusdelimiter" />
            <div id="whyustext">
                With these presets, and many more to come, you get the result of 20 years of photography and photoshop experience. We have carefully crafted each of these presets for a wide range of photography that we think you will find very useful on your photography.<br />
                Ange Loron
            </div>
        </div>
        <main>
            <div>
                {/* <h1 style={{textAlign: 'center'}}>WHY LORON PRESETS</h1>
                    Each presets were produced and tested by a team of photographers who each have over 15 years of professional experience.<br />
                    These presets are used on their day to day workflow of those professionals but we don't like to brag, so we will let others do it for us<br /> */}
            </div>
            {items.map((item, index) => <Displayproduct key={index} item={item} history={history} />)}
        </main>
    </>
}

export default LightroomPresets;