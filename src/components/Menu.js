import React, { useState } from 'react';
import { useStateValue } from "../State";
import {
    Toolbar,
    AppBar,
    Grid,
    Button,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    SwipeableDrawer,
    Badge,
    IconButton
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import {
    Menu as MenuIcon,
    Help as HelpIcon,
    Mail as MailIcon,
    ShoppingCart as ShoppingCartIcon,
    SettingsBrightness as SettingsBrightnessIcon,
} from '@material-ui/icons';
import CustomLink from "./CustomLink";
import Logo from "./Logo";
import "./css/Menu.css";

const Menu = ({ match }) => {

    const [{ cart }] = useStateValue();
    const [drawer, toggleDrawer] = useState(false);

    const MenuArray = [
        { name: 'Presets', icon: <SettingsBrightnessIcon />, link: 'LightroomPresets' },
        { name: 'FAQ', icon: <HelpIcon />, link: 'faq' },
        { name: 'Contact Us', icon: <MailIcon />, link: 'contact' },
    ]

    const StyledBadge = withStyles(theme => ({
        badge: {
            right: -3,
            top: 13,
            border: `2px solid ${theme.palette.background.paper}`,
            padding: '0 4px',
        },
    }))(Badge);

    return <>
        <AppBar style={{ color: '#2e2e2e', background: '#fff' }}>
            <Toolbar>
                <Grid
                    justify="space-between"
                    container
                >
                    <MenuIcon
                        id="drawertogglebutton"
                        onClick={() => toggleDrawer(!drawer)}
                    />
                    <Logo size={20} />
                    <div id="mainmenu">
                        {MenuArray.map((e, i) =>
                            <CustomLink key={i} to={`./${e.link}`}>
                                <Button
                                    startIcon={e.icon}
                                    className={match.params[0] === e.link ? 'active' : ( match.params[0] === '' && e.link === 'presets') ? 'active' : ''}
                                >
                                    {e.name}
                                </Button>
                            </CustomLink>
                        )}
                        <CustomLink to="/cart">
                            <IconButton aria-label="cart">
                                <StyledBadge badgeContent={cart.items.length} color="secondary">
                                    <ShoppingCartIcon />
                                </StyledBadge>
                            </IconButton>
                        </CustomLink>
                    </div>
                </Grid>
            </Toolbar>
        </AppBar>
        <SwipeableDrawer
            anchor="left"
            open={drawer}
            onClose={() => toggleDrawer(false)}
            onOpen={() => toggleDrawer(true)}
            id="drawer"
        >
            <Logo size={20} />
            <List>
                {MenuArray.map((e, i) => (
                    <CustomLink key={i} to={`./${e.link}`}>
                        <ListItem button onClick={() => toggleDrawer(false)} color="secondary">
                            <ListItemIcon>{e.icon}</ListItemIcon>
                            <ListItemText primary={e.name} />
                        </ListItem>
                    </CustomLink>
                ))}
            </List>
        </SwipeableDrawer>
    </>
}


export default Menu;