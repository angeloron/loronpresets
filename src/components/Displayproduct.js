import React, { useState } from "react";
import ReactGA from 'react-ga';
import ReactCompareImage from 'react-compare-image';
import { Link, Button } from "@material-ui/core/";
// import { Share } from "@material-ui/icons";
import { useStateValue } from "../State";
import { toProperCase } from "../Functions";
import CustomLink from "./CustomLink";
import "./css/Displayproduct.css";

const Displayproduct = ({ item, history }) => {
    const [{ cart }, dispatch] = useStateValue();
    const [preset, setpreset] = useState(0);

    return <div className="halfpage light" style={{ flexDirection: 'row-reverse' }}>
        <div id={item.name.replace(/ /g, "_").toLowerCase()} className="anchor" />
        {item.type === 'series' ?
            <div className="previewwrapper">
                <div className="imagecomparewrapper">
                    <ReactCompareImage
                        leftImage={`https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_400,f_auto/v1581273894/loronpresets/${item.name.replace(/ /g, "_").toLowerCase()}/original.png`}
                        rightImage={`https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_400,f_auto/v1581273894/loronpresets/${item.name.replace(/ /g, "_").toLowerCase()}/${item.content[preset].toLowerCase().replace(/ /g, "_")}.png`}
                    />
                </div>
                <div className="previewtabs">
                    {item.content.map((p, i) => <div
                        key={p}
                        onClick={() => setpreset(i)}
                        style={{ background: `url("https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_100,f_auto/v1581273894/loronpresets/${item.name.replace(/ /g, "_").toLowerCase()}/${item.content[i].toLowerCase().replace(/ /g, "_")}.png") no-repeat 50% 50%/cover` }}
                    />)}
                </div>
            </div>
            :
            <div className="previewwrapper">
                <div className="imagecomparewrapper">
                    <img
                        src={`https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_400,f_auto/v1581273894/loronpresets/${item.name.replace(/ /g, "_").toLowerCase()}/original.png`}
                        alt={item.name}
                    />
                </div>
            </div>
        }
        <div className="descriptionwrapper" itemScope itemType="http://schema.org/Offer">
            <h1>
                <div itemProp="name">{item.name} {toProperCase(item.type)}&nbsp;</div>
                <div itemProp="price"><b>
                    {item.discount ?
                        <><s>{item.price}{item.currency}</s> {item.discountprice}{item.currency}</>
                        :
                        <>{item.price}{item.currency}</>
                    }
                </b></div>
                {/* <a
                    data-pin-do="buttonPin"
                    data-pin-round="true"
                    data-pin-lang="en"
                    href={`https://www.pinterest.com/pin/create/button/?url=https%3A%2F%2Floronpresets.com%2F%23${item.name.replace(/ /g, "_").toLowerCase()}&media=https%3A%2F%2Floronpresets.com%2Fitems%2F${item.name.replace(/ /g, "_").toLowerCase()}%2Fcover.png&description=Mobile%2FDesktop%20Lightroom%20Presets%3A%20${item.name.replace(/ /g, "%20")}`}
                > </a> */}
            </h1>
            <img src={`https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_160,f_auto/v1581273894/loronpresets/${item.name.replace(/ /g, "_").toLowerCase()}/cover.png`} alt={item.name} />
            <h4>Included in this package:</h4>
            <ul>
                {item.content.map((p, i) => <li key={i}>{p}</li>)}
            </ul>
            {item.description} 
            <Link >
                <CustomLink to={`/product/${item.name.replace(/ /g,"-").toLowerCase()}`} style={{ display: "inline-block", cursor: "pointer" }}>
                    &nbsp;Read more...
                </CustomLink>
            </Link>
            {cart.items.includes(item.id) ?
                <Button
                    color="primary"
                    variant="contained"
                    onClick={() => {
                        history.push("/cart");
                    }
                    }
                >
                    CHECKOUT
                </Button>
                :
                <Button
                    color="secondary"
                    variant="contained"
                    onClick={() => {
                        dispatch({
                            type: 'addItem',
                            item: item.id
                        });
                        ReactGA.event({
                            category: "Add to cart",
                            action: `User added the ${item.name} ${item.type} to cart`,
                        });
                    }
                    }
                >
                    ADD TO CART
                </Button>
            }
            {/* <div style={{ float: "left" }}>
                <Button
                    size="small"
                    variant="outlined"
                    startIcon={<Share fontSize="small" />}
                > Share
                </Button>
            </div> */}
        </div>
    </div>;
}

export default Displayproduct;