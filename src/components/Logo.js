import React from "react";
import "./css/Logo.css";

const Logo = ({ size, style, ...props }) => {
    return <div className="mainlogo" >
        {/* <img src={`${process.env.PUBLIC_URL}/logo.png`} alt="logo" width={size * 2.1} height={size * 2.1} /> */}
        <img src={`https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_${size * 2.1},f_auto/v1581277352/loronpresets/logo.png`} alt="logo" width={size * 2.1} height={size * 2.1} />
        <div style={{ ...style }} {...props} >
            <div style={{ fontSize: size + (size / 2) }}>LORON</div>
            <div style={{ fontSize: size / 2, letterSpacing: size / 2 }}>PRESETS</div>
        </div>
    </div>
}

export default Logo;