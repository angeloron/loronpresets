import React, { useEffect, useState } from "react";
import { Button, Grid, Typography } from "@material-ui/core/";
import { Image } from 'cloudinary-react';
import { useStateValue } from "../State";
import ReactGA from 'react-ga';
import "./css/Productpage.css";

const Productpage = ({ history, items, ...props }) => {

    const [item, loaditem] = useState({});
    const [{ cart }, dispatch] = useStateValue();

    useEffect(() => {
        const name = history.location.pathname.replace("/product/", "").replace(/-/g, ' ');
        const selecteditems = items.filter(x => x.name.toLowerCase() === name.toLowerCase());
        loaditem(selecteditems[0])
    }, [item, items, history.location.pathname])

    return (item === undefined || item.name === undefined) ? <></> :
        <Grid container spacing={4} id="productpagewrapper" itemScope itemType="http://schema.org/Product">
            <Grid item xs={12} sm={8}>
                <main>
                    <header>
                        <Typography variant="h4" component="h1" itemProp="name">{item.name} Lightroom presets</Typography>
                    </header>
                    <p itemProp="description">{item.description}</p>
                    <Typography variant="h6">Here is a sample of each of these presets in use:</Typography>
                    <div className="displayphoto" style={{ width: "100%" }}>
                        <Image
                            publicId={`loronpresets/${item.name.replace(/ /g, '_').toLowerCase()}/original.png`}
                            width="590"
                            crop="scale"
                            quality="100"
                            fetchFormat="auto"
                            style={{ maxWidth: "100%" }}
                        />
                        <div className="displayphotocaption">ORIGINAL</div>
                    </div>
                    {item.type.toLowerCase() === "collection" ?
                        items.filter(x => x.type.toLowerCase() !== "collection").map(obj =>
                            obj.content.map(x =>
                                <div key={x} className="displayphoto">
                                    <Image
                                        publicId={`loronpresets/${obj.name.replace(/ /g, '_').toLowerCase()}/${x.replace(/ /g, '_').toLowerCase()}.png`}
                                        width="590"
                                        crop="scale"
                                        quality="100"
                                        fetchFormat="auto"
                                        style={{ maxWidth: "100%" }}
                                    />
                                    <div className="displayphotocaption">{x.toUpperCase()}</div>
                                </div>
                            )
                        )
                        :
                        item.content.map(x => <div key={x} className="displayphoto">
                            <Image
                                publicId={`loronpresets/${item.name.replace(/ /g, '_').toLowerCase()}/${x.replace(/ /g, '_').toLowerCase()}.png`}
                                width="590"
                                crop="scale"
                                quality="100"
                                fetchFormat="auto"
                                style={{ maxWidth: "100%" }}
                            />
                            <div className="displayphotocaption">{x.toUpperCase()}</div>
                        </div>
                        )}
                </main>
            </Grid>
            <Grid container item xs={12} sm={4} spacing={2} component="aside" alignContent="flex-start">
                <Grid item xs={6} sm={12}>
                    <Image
                        publicId={`loronpresets/${item.name.replace(/ /g, '_').toLowerCase()}/cover.png`}
                        width="150"
                        crop="scale"
                        quality="100"
                        fetchFormat="auto"
                        itemProp="image"
                    />
                    <meta itemProp="priceCurrency" content={item.currency} />
                    <Typography variant="h6" className="red">
                        {item.discount ?
                            <><s>{item.price}{item.currency}</s> <span itemProp="price">{item.discountprice}</span>{item.currency}</>
                            :
                            <><span itemProp="price">{item.price}</span>USD</>
                        }
                    </Typography>
                    {cart.items.includes(item.id) ?
                        <Button
                            color="primary"
                            variant="contained"
                            onClick={() => history.push("/cart")}
                        >
                            CHECKOUT
                            </Button>
                        :
                        <Button
                            color="secondary"
                            variant="contained"
                            onClick={() => {
                                dispatch({
                                    type: 'addItem',
                                    item: item.id
                                });
                                ReactGA.event({
                                    category: "Add to cart",
                                    action: `User added the ${item.name} ${item.type} to cart`,
                                });
                            }
                            }
                        >
                            ADD TO CART
                        </Button>
                    }
                </Grid>
                <Grid item xs={6} sm={12}>
                    <Typography variant="h6">includes:</Typography>
                    <ul style={{ margin: 0, padding: "0 15px" }}>
                        {item.content.map(x => <li key={x}>{x}</li>)}
                    </ul>
                </Grid>
            </Grid>
        </Grid>
}

export default Productpage;