import React from 'react';
import CustomLink from "./CustomLink";
import { useStateValue } from "../State";
import { Fab, } from '@material-ui/core';
import { Close as CloseIcon, } from '@material-ui/icons';
import "./css/GDPR.css";

const GDPR = () => {
    const [{ GDPR }, dispatch] = useStateValue();

    return !GDPR ?
        <></>
        :
        <div id="gdpr-consent-banner">
            <div>
                <div>
                    By using our site, you acknowledge that you have read and understand our <CustomLink to={`/cookiepolicy`}>Cookie Policy</CustomLink>, <CustomLink to={`/privacypolicy`}>Privacy Policy</CustomLink>, and our <CustomLink to={`/termsconditions`}>Terms & Conditions</CustomLink>.
                </div>
                <div>
                    <Fab color="primary" size="small" aria-label="close"
                        onClick={() => dispatch({
                            type: 'GDPR'
                        })}
                    >
                        <CloseIcon fontSize="small" />
                    </Fab>
                </div>
            </div>
        </div>
}


export default GDPR;