import React, { useState } from "react";
import { functions } from '../firebase/firebase';
import ReCAPTCHA from "react-google-recaptcha";
import { TextField, Button } from "@material-ui/core";
import { SnackbarProvider, useSnackbar } from 'notistack';
import { Helmet } from 'react-helmet'
import "./css/Contact.css"

const Contact = () => {

    const [name, loadName] = useState('');
    const [email, loadEmail] = useState('');
    const [message, loadMessage] = useState('');
    const [submit, submitForm] = useState(false);
    const [captchaValue, captchaChange] = useState(null);
    const { enqueueSnackbar } = useSnackbar();

    const handlesubmit = e => {
        e.preventDefault();
        submitForm(true);
        if (name === '' || email === '' || message === '' || !captchaValue) {
            enqueueSnackbar('Please fill up all fields', { variant: 'error' });
            captchaChange(null);
        } else {
            const data = { name, email, message };
            captchaChange(null);

            let sendMessage = functions.httpsCallable('contactme');

            sendMessage(data).then(function (res) {
                enqueueSnackbar('Your message was sent', { variant: 'success' });
                setTimeout(() => {
                    loadName('');
                    loadEmail('');
                    loadMessage('');
                    submitForm(false);
                }, 1000);
            }).catch(function (error) {
                enqueueSnackbar('There was an error sending your message', { variant: 'error' });
            });
        };

    }

    return <>
        <Helmet>
            <title>Loron Presets - Contact US</title>
            <meta name="description" content="Contact the team at Loron Presets for any enquiries regarding our Lightroom mobile and desktop presets." />
        </Helmet>
        <form id="contactmeForm">
            <div id="contactmeHeader">CONTACT FORM</div>
            <div id="contactinputWrapper">
                <TextField
                    label="name"
                    variant="outlined"
                    color="primary"
                    value={name}
                    error={name === '' && submit}
                    onChange={e => loadName(e.target.value)}
                    required
                />
                <TextField
                    label="e-mail"
                    type="email"
                    variant="outlined"
                    value={email}
                    error={email === '' && submit}
                    onChange={e => loadEmail(e.target.value)}
                    required
                />
                <TextField
                    label="message"
                    variant="outlined"
                    value={message}
                    error={message === '' && submit}
                    onChange={e => loadMessage(e.target.value)}
                    rows="8"
                    multiline
                    required
                />
                <ReCAPTCHA
                    sitekey="6LcHANYUAAAAAIKWmF09ksvyLmuYvAK5irV1en93"
                    onChange={captchaChange}
                    style={{ margin: '0 auto' }}
                />
                <Button
                    disabled={!captchaValue}
                    variant="contained"
                    type="submit"
                    color="primary"
                    onClick={e => handlesubmit(e)}
                >
                    SEND
                </Button>
            </div>
        </form>
    </>
}

export default function IntegrationNotistack() {
    return (
        <SnackbarProvider maxSnack={1}>
            <Contact />
        </SnackbarProvider>
    );
}