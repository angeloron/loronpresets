import React from 'react';
import { Container, Grid, Link, Box, Typography, Divider } from "@material-ui/core";
import { Facebook, Twitter, Instagram } from "@material-ui/icons";
import { toProperCase } from "../Functions";
import CustomLink from "./CustomLink";
import "./css/Footer.css";

const Footer = () => {

    const footeritems = [
        {
            name: "legal",
            content: [
                { name: 'About', link: 'about', linktype: 'custom' },
                { name: 'Cookie Policy', link: 'cookiepolicy', linktype: 'custom' },
                { name: 'Privacy Policy', link: 'privacypolicy', linktype: 'custom' },
                { name: 'Terms & Conditions', link: 'termsconditions', linktype: 'custom' },
                { name: 'Sales & Refunds', link: 'salesrefunds', linktype: 'custom' },
            ]
        },
        {
            name: "site map",
            content: [
                { name: 'Presets', link: 'presets', linktype: 'custom' },
                { name: 'FAQ', link: 'faq', linktype: 'custom' },
                { name: 'Contact Us', link: 'contact', linktype: 'custom' }
            ]
        },
        {
            name: "social",
            content: [
                { name: 'facebook', link: 'https://facebook.com/loronpresets', icon: <Facebook fontSize="large" /> },
                { name: 'instagram', link: 'https://instagram.com/angeloronphoto', icon: <Instagram fontSize="large" /> },
                { name: 'twitter', link: 'https://twitter.com/ange_loron', icon: <Twitter fontSize="large" /> }
            ]
        },
        // {
        //     name: "other_sites",
        //     content: [
        //         { name: 'angeloron.com', link: 'https://angeloron.com' },
        //     ]
        // }
    ]

    return <div id="footer">
        <Container>
            <Grid container justify="space-around" alignContent="flex-start" style={{ margin: "40px 0px 20px 0" }}>
                {footeritems.map((section, i) =>
                    <Grid item key={i}>
                        <Typography variant="body1">
                            {toProperCase(section.name.replace(/_/g, " "))}
                        </Typography>
                        {section.content.map((content, j) =>
                            content.linktype ?
                                <CustomLink key={j} to={`./${content.link}`}>
                                    <Link color="inherit">{content.name}</Link>
                                </CustomLink>
                                :
                                content.icon !== undefined
                                    ?
                                    <Link key={j} href={content.link} color="inherit" target="_blank" >
                                        {content.icon}
                                    </Link>
                                    :
                                    < Box key={j} >
                                        <Link href={content.link} color="inherit" target="_blank" >
                                            {content.name}
                                        </Link>
                                    </Box>
                        )}
                    </Grid>
                )}
            </Grid>
            <Divider color="inherit" />
            <Typography variant="subtitle2" style={{ background: "black", padding: "20px", textAlign: "center", color: "#5f6368" }}>
            {/* "initial","inherit","primary","secondary","textPrimary","textSecondary","error" */}
                Copyright © 2020 loronpresets. All rights reserved
            </Typography>
        </Container>
    </div >
}

export default Footer;