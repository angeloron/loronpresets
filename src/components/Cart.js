import React, { useEffect, useState } from 'react';
import { functions, db } from '../firebase/firebase';
import { makeStyles } from '@material-ui/core/styles';
import {
    Paper,
    Button,
    Typography,
    List,
    ListItem,
    ListItemText,
    CircularProgress
} from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import { useStateValue } from "../State";
import CustomLink from "./CustomLink";
import "./css/Cart.css";

const useStyles = makeStyles(theme => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    listItem: {
        padding: theme.spacing(1, 0),
    },
    total: {
        fontWeight: '700',
    },
    title: {
        marginTop: theme.spacing(2),
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
    },
}));

// const paylike = window.Paylike('6e7bd801-708f-464a-8503-fd523af85035'); //test
const paylike = window.Paylike('2dcbf3cf-93a1-4402-80da-2bed301304d6'); //live
const processpurchase = functions.httpsCallable('processpurchase');
const sendMessage = functions.httpsCallable('contactme');

const Cart = ({ items, history }) => {
    const classes = useStyles();
    const [{ cart }, dispatch] = useStateValue();
    const [selectedItems, loadItems] = useState([]);
    const [loading, setloading] = useState(false);

    useEffect(() => {
        loadItems(items.filter(item => cart.items.includes(item.id)));
    }, [cart.items, items])

    const amount = selectedItems.reduce((accum, item) => accum += item.discount ? item.discountprice : item.price, 0)

    const processTransaction = ({ transaction, custom }) => {

        setloading(true);

        const transactionid = transaction.id;
        const { email, name } = custom;
        db.collection("purchases").doc(transactionid).set({
            id: transactionid,
            amount,
            currency: "USD",
            date: new Date(),
            email,
            name,
        })
            .then(function () {
                console.log("Purchase recorded");
            })
            .catch(function (error) {
                console.error("Error saving that purchase: ", error);
            });

        processpurchase({ email, transactionid, selectedItems }).then(res => {
            dispatch({ type: 'emptyCart' });
            history.push("/thankyou");
            dispatch({
                type: 'addPurchase',
                purchase: { transactionid, items: res.data }
            })
            const data = { name: "purchaser", email: "purchase@loronpresets.com", message: "A new purchase was done on loronpresets.com." };
            sendMessage(data).then(() => { })
                .catch(function (error) {
                    console.error('Could not send email notification: ' + error);
                });
        }).catch(e => console.log(e))


    }

    return cart.items.length === 0 ?

        <main className={classes.layout}>
            <CustomLink to="/LightroomPresets">
                <Paper className={classes.paper} style={{ cursor: 'pointer' }}>
                    <Typography component="h1" variant="h5" align="center">
                        OOPS... YOUR CART IS EMPTY
                    </Typography>
                    <Typography style={{ textAlign: 'center' }}>
                        CLICK HERE TO GO SHOPPING
                    </Typography>
                </Paper>
            </CustomLink>
        </main>
        :
        loading
            ?
            <main className={classes.layout} >
                <Paper className={classes.paper}>
                    <CircularProgress size={100} style={{ display: 'flex', margin: 'auto' }} />
                </Paper>
            </main >
            :
            <>
                <main className={classes.layout}>
                    <Paper className={classes.paper}>
                        <Typography
                            component="h1"
                            variant="h4"
                            align="center"
                        >
                            CHECKOUT
                            </Typography>

                        <Typography variant="h6" gutterBottom>Order summary</Typography>
                        <List disablePadding>
                            {selectedItems.map(item => (
                                <ListItem className={classes.listItem} key={item.name}>
                                    <ListItemText
                                        className="hovercancel"
                                        primary={<div style={{ display: 'flex' }}>
                                            <CancelIcon
                                                style={{ fontSize: 18, cursor: 'pointer', margin: '2px' }}
                                                onClick={() => dispatch({
                                                    type: 'removeItem',
                                                    item: item.id,
                                                })}
                                            />
                                            {item.name} {item.type}
                                        </div>}
                                    />
                                    <Typography variant="body2">
                                        ${item.discount ? item.discountprice : item.price}
                                    </Typography>
                                </ListItem>
                            ))}
                            <ListItem className={classes.listItem}>
                                <ListItemText primary="Total" />
                                <Typography variant="subtitle1" className={classes.total}>
                                    ${amount}
                                </Typography>
                            </ListItem>
                        </List>
                        <div className={classes.buttons}>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => {
                                    paylike.popup({
                                        currency: 'USD',
                                        amount: amount * 100,
                                        fields: [
                                            'name',
                                            {
                                                name: 'email',
                                                label: 'E-mail',
                                                type: 'email',
                                                placeholder: 'user@example.com',
                                                required: true,
                                                value: '',
                                            },
                                        ],
                                    }, function (err, result) {
                                        if (err) return console.warn(err);
                                        processTransaction(result);
                                    });
                                }}
                                className={classes.button}
                            >PAY</Button>
                        </div>
                    </Paper>
                </main>
            </>
}

export default Cart;