import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CustomLink from "./CustomLink";
import "./css/FAQ.css";

const FAQ = () => {

    const [expanded, setExpanded] = React.useState(false);

    const handleChange = panel => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    return (
        <div id="FAQ">
            <h1>Frequently Asked Questions</h1>
            <ExpansionPanel expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1bh-content"
                    id="panel1bh-header"
                >
                    <Typography>
                        Do I need the Adobe Lightroom subscription to use the presets?
                    </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="faqdata">
                        <h4>MOBILE PRESETS:</h4>
                        You don't have to subscribe to Lightroom to use the presets on your mobile. I give you a tutorial to install them manually in the app (which is free)
                        <h4>DESKTOP PRESETS:</h4>
                        You need a version of Lightroom between LR 6 and LR CC to use the presets on desktop
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel3bh-content"
                    id="panel3bh-header"
                >
                    <Typography>How do you deliver the presets?</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="faqdata">
                        The presets are digital! You can get them from anywhere in the world.<br />
                        You will be prompted to enter your e-mail address while purchasing and that e-mail will be use to send you the files.
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel4bh-content"
                    id="panel4bh-header"
                >
                    <Typography>How do I install your presets?</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="faqdata">
                        <h4>Don't worry, We've got you covered!</h4>
                        Every purchase comes with a detailed tutorial on how to install our presets on Desktop and on Mobile.
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel4bh-content"
                    id="panel4bh-header"
                >
                    <Typography>Will your presets work on my phone?</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="faqdata">
                        If you were able to install Lightroom on your phone there is 99% chance that you can install our mobile presets.<br />
                        As long as you are able to receive e-mails on your phone and save the files attached to that e-mail, then you will be able to use our presets.
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel expanded={expanded === 'panel5'} onChange={handleChange('panel5')}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2bh-content"
                    id="panel2bh-header"
                >
                    <Typography>Can I use the same lightroom presets on my phone and my desktop computer?</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="faqdata">
                        Yes!<br /><br />
                        If you have Lightroom with a version between Lightroom 6 and Lightroom CC all our presets are compatible with both desktop and phones.<br />
                        If you are using Lightroom CC you can synchronise the presets so once installed on your computer it will appear on your Lightroom phone app.
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel expanded={expanded === 'panel6'} onChange={handleChange('panel6')}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2bh-content"
                    id="panel2bh-header"
                >
                    <Typography>How can I get a refund?</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="faqdata">
                        Due to the digital nature of our products, we do not offer refunds.<br />
                        Except if you purchased an item several times by mistake.<br />
                        In which case you can contact us <div className="contactmelink"><CustomLink to={'/contact'}>here</CustomLink></div>.
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel expanded={expanded === 'panel7'} onChange={handleChange('panel7')}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2bh-content"
                    id="panel2bh-header"
                >
                    <Typography>I have a problem! Help! Nothing works!</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="faqdata">
                        <h4>Don't worry.</h4>
                        If you are having a problem our technical team will be more than willing to assist you.<br />
                        Send us all the details of the problem you ran into <div className="contactmelink"><CustomLink to={'/contact'}>here</CustomLink></div>.<br />
                        And we will rapidly service you.
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel expanded={expanded === 'panel8'} onChange={handleChange('panel8')}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2bh-content"
                    id="panel2bh-header"
                >
                    <Typography>I love Loron Presets and would like to help you guys. What can I do?</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="faqdata">
                        <h4>WOW! THANK YOU SO MUCH!</h4>
                        We love you too and in fact, we love all our customers and take pride in servicing them with everything we can.<br />
                        If you feel that you can and want to do more to support us, here are some options:<br /><br />
                        <h5>FINANCIAL SUPPORT</h5>
                        Well, we won't lie, this type of support is always very welcome as we could do so much more if we had the funds necessary.<br />
                        You can donate to LORONPRESETS by clicking <a itemProp="url" href="https://pos.paylike.io/?key=2dcbf3cf-93a1-4402-80da-2bed301304d6&currency=USD&reference=loronpresets" target="_blank" rel="noopener noreferrer">here</a>.
                        You can select the currency and amount that you want to donate.<br /><br />
                        <h5>PROMOTIONAL SUPPORT</h5>
                        You want to promote our products and website?<br />
                        We are working on making banners and other promotional pieces that you can download.<br />
                        For now the easiest way is to contact us <div className="contactmelink"><CustomLink to={'/contact'}>here</CustomLink></div> and we'll work something out.<br /><br />
                        {/* You can download our promotional banners <div className="contactmelink">here</div>.<br /><br /> */}
                        <h5>WORKFORCE SUPPORT</h5>
                        You are also a passionate and want to join our team?<br />
                        You better be ready for some exciting time then! Contact us <div className="contactmelink"><CustomLink to={'/contact'}>here</CustomLink></div> immediately.<br />
                        We can't wait to hear from you to expand our team!<br /><br />
                        <h5>ADVICE SUPPORT</h5>
                        If you've got some suggestions/advice for us or even some critique, please go ahead and send it to us <div className="contactmelink"><CustomLink to={'/contact'}>here</CustomLink></div>.<br />
                        We all want to improve and are aware that there is a lot to learn, definitely feel free to write us anything.<br />
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel expanded={expanded === 'panel9'} onChange={handleChange('panel9')}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2bh-content"
                    id="panel2bh-header"
                >
                    <Typography>Where is loronpresets managed and who is behind it?</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="faqdata">
                        Loronpresets was created by Ange Loron in Denmark.<br />
                        We are currently registered at<br /><br />
                        Karlslunde Strandveij<br />
                        2690 Karlslunde<br />
                        Denmark<br /><br />

                        To find out more about Ange Loron, you can visit his portfolio <a href="https://angeloron.com">here</a><br />
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        </div>
    );
}

export default FAQ;