import React from "react";
import { Box, Grid, Typography } from "@material-ui/core";

const PageNotFound = () => {

    return <Grid container justify="center" alignItems="center" style={{ height: "calc(100vh - 125px)" }}>
        <Grid
            container
            item
            direction="column"
            justify="center"
            alignItems="center"
            style={{
                height: "100%",
                // backgroundImage: "url(https://images.unsplash.com/photo-1487088678257-3a541e6e3922?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80)"
            }}
        >
            <Box style={{
                fontSize: "26vw",
                fontWeight: 800,
                fontFamily: "sans-serif",
                backgroundImage: "url(https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_1500,f_auto/v1581273894/loronpresets/landscape/a_high_contrast_of_nature.png)",
                backgroundSize: "cover",
                backgroundClip: "text",
                WebkitBackgroundClip: "text",
                color: "transparent",
                WebkitTextStroke: "1px black",
            }}
            >
                4
                <img
                    style={{ height: "20vw" }}
                    src={`https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_350,f_auto/v1581277352/loronpresets/logo.png`}
                    alt="logo"
                />
                4
            </Box>
            <Typography variant="h5">
                Oops... This page does not exist.
            </Typography>
        </Grid>
    </Grid >
}

export default PageNotFound;