import React from "react";

const Howtoinstallpresets = () => {

    return <>
        <header class="page-header">
            <h1>How To Install Lightroom Presets</h1>
        </header>
        <div class="page-content">
            <p><span>Welcome to Pretty Presets for Lightroom!  We have the best Lightroom preset install video and written tutorial below as well as the <a href="https://www.facebook.com/groups/prettypresets/" target="_blank" title="Pretty Presets Lightroom Facebook Group" rel="noopener noreferrer">#1 Lightroom Preset Community</a>. These basic instructions will teach you <strong>How to Install Lightroom Presets</strong> into Adobe Lightroom in just 2-3 minutes. We'll have you using your wonderful presets in no time!</span></p>
            <p><span>First, let's establish which version of Lightroom you are currently using.  Things are a little confusing at Adobe right now.  There are so many versions of Lightroom (with almost the same name!) and preset files types.  Read below to see which option will work best for you.</span></p>
            <p><span><span>Looking for a helpful video for installing your presets? Learn more here! </span><a href="https://youtu.be/gEbb5EEIRV8" target="_blank" title="How to Install Lightroom Presets" rel="noopener noreferrer">How to install presets on a Mac or PC</a><span> . Not sure what version of Lightroom you're using?  Click "<strong>Help</strong>" at the top of your Lightroom dashboard and then "<strong>System Info</strong>" to find out!</span></span></p>
            <h2><span>Lightroom Classic</span></h2>
            <p>
                <strong>1. Lightroom Classic (version 7.0 and up - xmp files):</strong>
                <span> </span>
                <span><em>Scroll down for instructions and video.</em></span><br />
                <strong>2. Lightroom Classic (version 4.0-7.0 - lrtemplate files):</strong>
                <span> </span><a href="https://www.lightroompresets.com/blogs/pretty-presets-blog/7403760-video-how-to-install-lightroom-presets-and-lightroom-brushes" title="How to install .lrtemplate presets files in Lightroom Classic CC">Follow this link for instructions and video.</a>
            </p>
            <ul></ul>
            <h2><span>Lightroom CC</span></h2>
            <p><strong>1. Lightroom CC desktop app:</strong> <a href="https://www.lightroompresets.com/blogs/pretty-presets-blog/installing-your-pretty-presets-in-adobe-lightroom-cc-2017-update" title="How to Install Lightroom Presets in the Lightroom CC desktop app"><em>Follow this link for written instructions and video.</em></a> <br /><span><strong>2. Lightroom CC on the mobile app (DNG files):</strong> <em><a href="https://www.lightroompresets.com/blogs/pretty-presets-blog/how-to-install-presets-in-free-lightroom-mobile-app" title="How to install Lightroom Presets in Lightroom cc mobile app—DNG files">Follow this link for written instructions and video.</a></em></span></p>
            <h2><span>Lightroom 4, 5 and 6</span></h2>

            <p><span><strong>1. Lightroom 6 (or older):</strong><em><span> </span><a href="https://youtu.be/gEbb5EEIRV8" title="How to install lightroom presets in Lightroom 6 or older versions of Lightroom">Follow this link for video tutorial.</a></em></span></p>
            <p><span><a href="https://www.facebook.com/groups/prettypresets/" title="Best Lightroom Group on Facebook">
                <img alt="Lightroom Facebook Group" src="https://cdn.shopify.com/s/files/1/0163/6622/files/Best_Lightroom_Group_on_Facebook3.jpg?v=1537040147" />
            </a></span></p>
            <h2><span>How to Install Your Lightroom Presets from Pretty Presets in the Most Recent Version of Lightroom Classic CC (.xmp files)</span></h2>
            <p><span>1. </span><strong>On a PC</strong><span>, go into Lightroom and click on </span><span><strong>Edit</strong> at the top (next to File), then click on Preferences</span><span>. <strong>On a Mac</strong>, click on the </span><strong>Lightroom tab</strong><span> (to the left of File), then click on </span><span>Preferences</span><span>.</span></p>

            <p><span><img src="https://cdn.shopify.com/s/files/1/0163/6622/files/PrettyPresetsInstall_1024x1024.jpg?v=1516580731" alt="How do I install Lightroom Presets" /></span></p>

            <p><span>2.  There will be a new screen that pulls up and there will be several tabs at the top, click on <strong>Presets</strong> (the second tab).</span></p>

            <p><span>3.  Click on the box titled, <strong>Show Lightroom Develop Presets. </strong></span></p>
            <p><span><img src="//cdn.shopify.com/s/files/1/0163/6622/files/Show_Lightroom_Develop_Presets.png?v=1540090341" alt="Show Lightroom Develop Presets" /></span></p>

            <p><span>4.  Double click on <strong>Settings</strong>. Paste the folder from your download that holds the .xmp files into this folder.</span></p>

            <p><span><img alt="How to Install Lightroom Presets Windows" src="https://cdn.shopify.com/s/files/1/0163/6622/files/08_grande.jpg?9502"  /></span></p>

            <p><span>5.  You're done!  If Lightroom was open when you copied the Pretty Presets, you will have to close it and restart it.</span></p>
            <h4><span>Need additional help installing your Pretty Presets XMP Files in Lightroom? Check out the video below!</span></h4>
            
            <div class="page" title="Page 1">
            
            </div>
            <div class="layoutArea">
                <div class="column"></div>
                <p class="p2"><a href="https://www.lightroompresets.com" title="Lightroom Presets">
                    <img alt="Lightroom Presets from Pretty Presets" src="https://cdn.shopify.com/s/files/1/0163/6622/files/Lightroom_Presets_Film_bohemian.png?v=1525469028" />
                </a></p>
                <h2><span></span></h2>
                <h2>Lightroom Presets Troubleshooting Guide - possible issues and their solutions!</h2>
                <div class="page" title="Page 4">
                    <h4 class="column"><span>Preset Download Issues:</span></h4>
                    <ul>
                        <li>If the download isn't working, try a different internet browser.</li>
                        <li>Do NOT choose to “open” the file after download. Just hit save, then close that dialogue box. Then go open your Windows Explorer window and locate the zip file, which is most likely in your Downloads folder. Extract the file according to instructions in video tutorial or cheat sheet below.</li>
                        <li>Can't Unzip? If on a PC right clicking does not offer “extract files”, then you may need to download an external program to unzip. We recommend trying WinRar, a free unzipping program.</li>
                        <li>Still having trouble? Contact us at support@prettypresets.freshdesk.com for further assistance </li>
                    </ul>
                    <h4><span>Presets Not Showing Up in Lightroom</span></h4>
                    <div class="layoutArea">
                        <div class="column">
                            <ul>
                                <li>Try shutting down Lightroom and then restart it.</li>
                                <li>If the preset collection name shows in Lightroom but not the individual presets, click the triangle to the left of the collection name and the templates will drop down.</li>
                                <li>Make sure the folder you copied into the Develop Presets folder actually holds the presets and not <strong>another folder</strong>.</li>
                                <li>Make sure you copied the right presets into the right folder.</li>
                                <li>Do NOT check “Store Presets With Catalog.” This setting is found to the left of "Show Lightroom Presets Folder" in your Preferences menu. If you have multiple catalogs, your presets will not show up in additional ones.</li>
                                <li>Make sure that you EXTRACTED the zip file. If you merely double clicked on the folder (on a PC), you will get access to the presets, but those will NOT be extracted files and will NOT copy/paste correctly and will NOT show up in your Lightroom.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="page" title="Page 4">
                    <div class="layoutArea">
                        <div class="column">
                            <h2>Lightroom Preset Installation Cheat Sheet</h2>
                            <p>If you would like a physical copy of our <a href="http://www.mediafire.com/file/bh9b1kcau9u1mfg/Develop%20Presets%20Installation%20Guide.pdf" target="_blank" title="Download Instructions for Installing Lightroom Presets" rel="noopener noreferrer">preset install instructions</a> you can enjoy them here! </p>
                            <div itemProp="video" itemScope="" itemType="http://schema.org/VideoObject">
                                <h2><span itemProp="name">Tutorial Video: How to Install Presets in an Old Version of Lightroom on a MAC and PC</span></h2>
                                <div id="schema-videoobject" class="video-container">
                                    
                                </div>
                            </div>
                            <div itemProp="video" itemScope="" itemType="http://schema.org/VideoObject"><span itemProp="description"></span></div>
                            <div itemProp="video" itemScope="" itemType="http://schema.org/VideoObject"><span itemProp="description"></span></div>
                            <ul></ul>
                            <h2>More Helpful links to get you started:</h2>
                            <ul>
                                <li><a href="https://www.lightroompresets.com/pages/how-to-install-lightroom-brushes" title="How to Install Lightroom Brushes">Looking for help installing brushes in Lightroom?</a></li>
                                <li><a href="https://www.lightroompresets.com/collections/lightroom-presets" title="Pretty Presets for Lightroom Preset Collections">Looking for our top Lightroom Presets?</a></li>
                                <li><a href="https://www.lightroompresets.com/pages/pretty-new-start-here" title="New to Pretty Presets?  Start Here.">New to Pretty Presets?  Start Here!</a></li>
                            </ul>
                            <p>Be sure to come join our <a href="https://www.facebook.com/groups/prettypresets/" class="customize-unpreviewable">Pretty Presets group</a> on Facebook for access to 10 mentors and over 55,000 members that love to encourage and help you with Lightroom and Pretty Presets.  See you there!</p>
                            <p><strong><span>We love our community and are a trusted resource to over 250,000 photographers from over 100+ countries.  Pretty Presets has been in business for 8+ years now, and we look forward to serving you. Let us know how we can help!</span></strong></p>
                            <p class="p2"><a href="https://www.lightroompresets.com/products/clean-edit-portrait-lightroom-presets"><img alt="clean" src="//cdn.shopify.com/s/files/1/0163/6622/files/Clean_Edit_Portrait_Workflow_skinny_2_1024x1024.jpg?v=1522044294" width="1024x1024" height="1024x1024" /></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>
}

export default Howtoinstallpresets;