import React from "react";
import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import { faFacebookF, faInstagram, faTwitter, faLinkedin, fa500px, faYoutube } from '@fortawesome/free-brands-svg-icons';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import useTypewriter from "./Typewriter";
import './css/About.css';

const About = () => {

    const [typewriter] = useTypewriter(['Photographer', 'Freelancer',  'Passionate', 'Web Developer',]);

    return <>
        <div id="aboutmeWrapper">
            <img id="aboutmePhoto" src={`./${process.env.PUBLIC_URL}/angeloron.jpg`} alt="Ange Loron" />
            <div id="aboutmeBox">
                <h2>Hi, I'm Ange Loron and I am a <div id="aboutmeTypewriter">{typewriter}</div></h2>
                <div id="aboutmeShort" style={{ lineHeight: '28px', color: 'rgb(136, 136, 136)' }}>
                    <p>With several years of experience as a web developer and designer I got familiar with Photoshop and from there I developed a passion on photography.</p>
                    <p>The art of photography is fabulous and something I love doing but I never had much time to edit my shots.
                    Until I discovered the beauty of Ligthroom and its presets!
                    My team and I are pleased to share our favorite presets with the rest of the world to help passionates like us to keep evolving in this busy world.</p>

                    Find me elsewhere on the world wide web:
                    <div id="socialWrapper">
                        <div className="social" id="loronangesite" onClick={() => window.open('https://www.loronange.com', '_blank')}>
                            <AccountCircleIcon />
                        </div>
                        <div className="social" id="instagram" onClick={() => window.open('https://www.instagram.com/imacange', '_blank')}>
                            <Icon id="instagramIcon" icon={faInstagram} />
                            <div id="instagramBackground" />
                        </div>
                        <div className="social" id="fivehundredpx" onClick={() => window.open('https://500px.com/imacange', '_blank')}>
                            <Icon icon={fa500px} />
                        </div>
                        <div className="social" id="facebook" onClick={() => window.open('https://www.facebook.com/loronange', '_blank')}>
                            <Icon icon={faFacebookF} />
                        </div>
                        <div className="social" id="twitter" onClick={() => window.open('https://twitter.com/imacange', '_blank')}>
                            <Icon icon={faTwitter} />
                        </div>
                        <div className="social" id="linkedin" onClick={() => window.open('https://www.linkedin.com/in/ange-loron-13047050', '_blank')}>
                            <Icon icon={faLinkedin} />
                        </div>
                        <div className="social" id="youtube" onClick={() => window.open('https://www.youtube.com/channel/UC7JUDc1VqGpR93F0CaOQRWg', '_blank')}>
                            <Icon icon={faYoutube} />
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </>
};

export default About;