import React from 'react'
import { useStateValue } from "../State";
import { Paper } from '@material-ui/core';
import "./css/Thankyou.css";

const Thankyou = () => {

    const [{ purchases }] = useStateValue();

    console.log(purchases);

    return purchases.length === 0
        ?
        <main>
            <Paper style={{ width: '600px', margin: '40px auto', padding: '40px' }}>
                <h1 style={{ marginTop: '0px' }}>Strange...</h1>
                It does not seem like you bought anything yet.
            </Paper>
        </main>
        :
        <main>  
            {purchases.map((purchase, i) =>
                <Paper key={i} style={{ width: '600px', margin: '40px auto', padding: '40px' }}>
                    <h1 style={{ marginTop: '0px' }}>Thank You for your purchase.</h1>
                    You purchase ID is <b>{purchase.transactionid}</b>.<br />
                    You can download your purchase for the next 24hrs from the following links:<br />
                    {purchase.items.map((item, i) =>
                        <div className="purchaseditems" key={i}>
                            <h4>{item.name}</h4>
                            Mobile format: <a itemprop="url" href={item.signedUrls.mobileUrl} >here</a><br />
                            Desktop format: <a itemprop="url" href={item.signedUrls.desktopUrl}>here</a>
                        </div>
                    )}
                    <br /><br />
                    You should have received an email with these links as well.<br />
                    <h4>Enjoy your new presets.</h4>
                </Paper>
            )}
        </main>
}

export default Thankyou