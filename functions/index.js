const EmailTemplate = require('./EmailTemplate');
const admin = require('firebase-admin');
const functions = require('firebase-functions');
// const paylike = require('paylike')("600e1502-857d-4d22-b679-9f74b94ba658"); //test
const paylike = require('paylike')("ad941d1c-19ab-4391-803d-419d7bb3a3bc"); //live
const nodemailer = require('nodemailer');
const cors = require('cors')({ origin: true });
const sgMail = require('@sendgrid/mail');
const SENDGRID_API_KEY = functions.config().sendgrid.key;
const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: gmailEmail,
    pass: gmailPassword
  }
});

admin.initializeApp(functions.config().firebase);

const generateUrl = (type, name, expires) => {
  return new Promise(async resolve => {
    const link = admin.storage().bucket().file(`presets/${name}/${name} ${type}.zip`);
    const signedUrl = await link.getSignedUrl({ action: 'read', expires });
    resolve(signedUrl);
  })
}

const generateBothUrls = (item, expires) => {
  const { name } = item
  return new Promise(async resolve => {
    const [mobileUrl] = await generateUrl('mobile', name, expires)
    const [desktopUrl] = await generateUrl('desktop', name, expires)
    item.signedUrls = { mobileUrl, desktopUrl }
    resolve(item)
  })
}

const generateSignedUrls = data => {
  const date = new Date();
  date.setDate(date.getDate() + 1);
  return new Promise(async resolve => {
    const promiseArray = data.map(item => generateBothUrls(item, date))
    const newData = await Promise.all(promiseArray)
    resolve(newData)
  })
}

exports.processpurchase = functions.region('europe-west1').https.onRequest((req, res) => {
  cors(req, res, async () => {
    const { email, transactionid, selectedItems } = req.body.data;
    const returnData = await generateSignedUrls(selectedItems);
    const amount = returnData.reduce((accum, item) => accum += item.price, 0);
    const data = {
      amount: amount * 100,
      currency: 'USD',
      descriptor: 'loronpresets purchase',
    }
    paylike.transactions.capture(transactionid, data)
      .then(() => console.log(`${amount}USD was captured from transaction id: ${transactionid}`), e => console.log(`failed: ${e}`))

    sendPurchaseEmail(email, transactionid, returnData)
    res.status(200).send({ data: returnData })
  });
})

const sendPurchaseEmail = (email, transactionid, data) => {
  sgMail.setApiKey(SENDGRID_API_KEY);
  const msg = {
    to: email,
    replyto: "noreply@loronpresets.com",
    from: "noreply@loronpresets.com",
    subject: `Your purchase from loronpresets.com`,
    text: EmailTemplate.purchaseack.text(data, transactionid),
    html: EmailTemplate.purchaseack.html(data, transactionid),
  };

  sgMail.send(msg)

};

exports.contactme = functions.region('europe-west1').https.onRequest((req, res) => {
  cors(req, res, () => {
    if (req.method !== 'POST') {
      return;
    }

    const mailOptions = {
      from: req.body.data.email,
      replyTo: req.body.data.email,
      to: gmailEmail,
      subject: `From ${req.body.data.name}`,
      text: `Email: ${req.body.data.email}
            Message: ${req.body.data.message}`,
      html: `Email: ${req.body.data.email} <br /> Message: ${req.body.data.message}`
    };

    mailTransport.sendMail(mailOptions);

    res.status(200).send({ data: { isEmailSend: true } });
  });
});