//UPLOAD BY RUNNING THIS PAGE THROUH CMD WITH "node jsontofirebase.js"

const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: "AIzaSyCwC28cTk1fAFAgEB0qdziDY20eXZbfOIo",
    authDomain: "loronpresets.firebaseapp.com",
    projectId: "loronpresets",
});

var db = firebase.firestore();

items.forEach(function (obj) {
    const { id, name, description, price, type, title, content, currency, discount, discountprice, discountcurrency } = obj;
    db.collection("items").add({ //WRITE THE NAME OF THE COLLECTION
        id,
        type,
        name,
        title,
        description,
        price,
        content,
        currency,
        discount,
        discountprice,
        discountcurrency,
    }).then(function (docRef) {
        console.log("Document written with ID: ", docRef.id);
    })
        .catch(function (error) {
            console.error("Error adding document: ", error);
        });
});


//ARRAY TO UPLOAD
var items = [
    {
        id: 100,
        type: 'collection',
        name: 'Master',
        title: `<h2 className="red">-50% ON ALL OUR SERIES</h2>`,
        description: `ALL OUR LIGHTROOM PRESETS IN ONE MASTER COLLECTION!<br />
            Compatible for Mobile and Desktop. You will have over 50 presets for half the price.`,
        price: 120,
        content: [
            '8 Bring to life presets',
            '8 Dusk til dawn presets',
            '8 Landscape presets',
            '8 Vintage presets',
            '8 Wedding presets',
        ],
        currency: 'USD',
        discount: false,
        discountprice: 100,
        discountcurrency: 'USD',
    },
    {
        id: 1,
        type: 'series',
        name: 'Bring to Life',
        title: '<h2 className="red">-50% ON ALL OUR SERIES</h2>',
        description: `This unique set of Presets pack contains some of my 8 favorite minimalist presets.
                  They were put together in one pack for you to use it whenever you need.
                  Perfect for out doors, landscapes or simply adding some natural light.`,
        price: 45,
        content: [
            'A light touch',
            'A touch of the sea',
            'Awesome sepia',
            'Black Blue touch',
            'The light pastel moment',
            'Color punch',
            'October day',
            'You got the look',
        ],
        currency: 'USD',
        discount: false,
        discountprice: 25,
        discountcurrency: 'USD',
    },
    {
        id: 2,
        type: 'series',
        name: 'Dusk til Dawn',
        title: '<h2 className="red">-50% ON ALL OUR SERIES</h2>',
        description: `This unique set of Presets pack contains 8 presets that are perfect to transform the usual grey winter into an amazing white winter.
            Perfect for pictures with snow, at the mountain, or simply adding some cold to your picture.`,
        price: 45,
        content: [
            'Pure vivid dusk',
            'Bad dream',
            'Contrast punch',
            'Dream dusk',
            'Dusk punch',
            'That was yesterday',
            'Viva la vida',
            'Warm new world',
        ],
        currency: 'USD',
        discount: false,
        discountprice: 25,
        discountcurrency: 'USD',
    },
    {
        id: 3,
        type: 'series',
        name: 'Landscape',
        title: '<h2 className="red">-50% ON ALL OUR SERIES</h2>',
        description: `This unique set of Presets pack contains 8 presets that are perfect to transform the usual grey winter into an amazing white winter.
            Perfect for pictures with snow, at the mountain, or simply adding some cold to your picture.`,
        price: 45,
        content: [
            'A high contrast of nature',
            'Awesome punch',
            'Blue fear',
            'Cross process nature',
            'I wish you were here',
            'Mountains of yesterday',
            'Natural Vibrance',
            'The kingdom',
        ],
        currency: 'USD',
        discount: false,
        discountprice: 25,
        discountcurrency: 'USD',
    },
    {
        id: 4,
        type: 'series',
        name: 'Vintage',
        title: '<h2 className="red">-50% ON ALL OUR SERIES</h2>',
        description: `This unique set of Presets pack contains 8 presets that are perfect to transform the usual grey winter into an amazing white winter.
            Perfect for pictures with snow, at the mountain, or simply adding some cold to your picture.`,
        price: 45,
        content: [
            'A toast to vintage',
            'Back to the past',
            'Come with me',
            'Endless love',
            'Green admiration',
            'Nostalgia boost',
            'Remember the time',
            'Welcome to the 70s',
        ],
        currency: 'USD',
        discount: false,
        discountprice: 25,
        discountcurrency: 'USD',
    },
    {
        id: 5,
        type: 'series',
        name: 'Wedding',
        title: '<h2 className="red">-50% ON ALL OUR SERIES</h2>',
        description: `This unique set of Presets pack contains 8 presets that are perfect to transform the usual grey winter into an amazing white winter.
                      Perfect for pictures with snow, at the mountain, or simply adding some cold to your picture.`,
        price: 45,
        content: [
            'A bright touch',
            'Boost and impinge',
            'Immortal ring',
            'Peach memory',
            'Speechless',
            'The best days',
            'The bright star',
            'The matte effect',
        ],
        currency: 'USD',
        discount: false,
        discountprice: 25,
        discountcurrency: 'USD',
    },
];