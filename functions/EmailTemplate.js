module.exports.purchaseack = {
  html: (data, transactionid) => `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml"><head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
      <!--[if !mso]><!-->
      <meta http-equiv="X-UA-Compatible" content="IE=Edge">
      <!--<![endif]-->
      <!--[if (gte mso 9)|(IE)]>
      <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <!--[if (gte mso 9)|(IE)]>
  <style type="text/css">
    body {width: 600px;margin: 0 auto;}
    table {border-collapse: collapse;}
    table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
    img {-ms-interpolation-mode: bicubic;}
  </style>
<![endif]-->
<link href="https://fonts.googleapis.com/css?family=Raleway:100&display=swap" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" rel="stylesheet"> 
      <style type="text/css">
    body, p, div {
      font-family: 'Roboto', sans-serif;
      font-size: 16px;
    }
    body {
      color: #FFFFFF;
    }
    body a {
      color: #08D3FF;
      text-decoration: none;
    }
    p { margin: 0; padding: 0; }
    table.wrapper {
      width:100% !important;
      table-layout: fixed;
      -webkit-font-smoothing: antialiased;
      -webkit-text-size-adjust: 100%;
      -moz-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }
    img.max-width {
      max-width: 100% !important;
    }
    .column.of-2 {
      width: 50%;
    }
    .column.of-3 {
      width: 33.333%;
    }
    .column.of-4 {
      width: 25%;
    }
    @media screen and (max-width:480px) {
      .preheader .rightColumnContent,
      .footer .rightColumnContent {
        text-align: left !important;
      }
      .preheader .rightColumnContent div,
      .preheader .rightColumnContent span,
      .footer .rightColumnContent div,
      .footer .rightColumnContent span {
        text-align: left !important;
      }
      .preheader .rightColumnContent,
      .preheader .leftColumnContent {
        font-size: 80% !important;
        padding: 5px 0;
      }
      table.wrapper-mobile {
        width: 100% !important;
        table-layout: fixed;
      }
      img.max-width {
        height: auto !important;
        max-width: 100% !important;
      }
      a.bulletproof-button {
        display: block !important;
        width: auto !important;
        font-size: 80%;
        padding-left: 0 !important;
        padding-right: 0 !important;
      }
      .columns {
        width: 100% !important;
      }
      .column {
        display: block !important;
        width: 100% !important;
        padding-left: 0 !important;
        padding-right: 0 !important;
        margin-left: 0 !important;
        margin-right: 0 !important;
      }
    }
  </style>
      <!--user entered Head Start-->

     <!--End Head user entered-->
    </head>
    <body>
      <center class="wrapper" data-link-color="#fe5d61" data-body-style="font-size:16px; font-family:'Roboto', sans-serif; color:#FFFFFF; background-color:#f2f4fb;">
        <div class="webkit">
          <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#f2f4fb">
            <tbody><tr>
              <td valign="top" bgcolor="#f2f4fb" width="100%">
                <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                  <tbody><tr>
                    <td width="100%">
                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tbody><tr>
                          <td>
                            <!--[if mso]>
    <center>
    <table><tr><td width="600">
  <![endif]-->
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width:100%; max-width:600px;" align="center">
                                      <tbody><tr>
                                        <td role="modules-container" style="padding:0px 0px 0px 0px; color:#FFFFFF; text-align:left;" bgcolor="#f2f4fb" width="100%" align="left"><table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
    <tbody><tr>
      <td role="module-content">
        
      </td>
    </tr>
  </tbody></table><table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="98ndJyAY9BSGjoVqrr6FYx">
      <tbody><tr>
        <td style=" font-size:6px;
                    line-height:10px;
                    padding:2% 0px;
                    height: 20%;
                    width: 15%;"                    
                    valign="top"
                    align="left"
        >
          <img class=""
          border="0"
          style=" display: block; max-width:140px;
                  width:100%;
                  height:auto;"
        src="https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_200/v1581277352/loronpresets/logo.png"
        alt="Logo"
        data-responsive="true"
        data-proportionally-constrained="false"
        >
          
        </td>
        <td style="color:#000000;
                  text-decoration:none;
                  font-family: Raleway,sans-serif;
                  font-size: 3vh;
                  width: 85%;
                  padding-left: 1%;
                  line-height: 100%;"
        >
        <div style="font-family: Raleway,sans-serif; font-size:150%">LORON</div>
        <div style="font-family: Raleway,sans-serif; font-size:100%; letter-spacing: 0.45vh;">PRESETS</div>
        </td>
      </tr>
    </tbody></table><table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="3Ypdby9Xfsf2rN27zTDEfN">
      <tbody><tr>
        <td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
          <img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:'Roboto', sans-serif; font-size:16px; max-width:100% !important; width:100%; height:auto !important;" src="https://res.cloudinary.com/angeloron/image/upload/c_scale,q_100,w_1500/v1581273894/loronpresets/master//original.png" alt="" width="600" data-responsive="true" data-proportionally-constrained="false">
        </td>
      </tr>
    </tbody></table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="7pyDCmyDaGcm5WsBBSaEgv">
      <tbody><tr>
        <td style="background-color:#2e2e2e; padding:50px 0px 30px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="#FE5D61"><div><div style="font-family: inherit; text-align: center"><span style="font-size: 24px">
        Thank you for your purchase!
        </span></div><div></div></div></td>
      </tr>
    </tbody></table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="nSVYnVzPLnGZ4wUdynLiKo">
      <tbody><tr>
        <td style="background-color:#2e2e2e; padding:30px 50px 30px 50px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="#2e2e2e"><div><div style="font-family: inherit; text-align: center">
        You can download your new presets on the links below and immediately start using them.<br />
        </div>
<div style="font-family: inherit; text-align: center">&nbsp;</div>
<div style="font-family: inherit; text-align: center">
These links are only available for 24hrs, so be fast and if you were to delete the files by mistake, no worry, you can always contact us via <a href="//loronpresets.com" target="_blank">loronpresets.com</a> and provide us with your email address and purchase number (${transactionid}) to get a new link.
</div>
<div style="font-family: inherit; text-align: center">&nbsp;</div>
<div style="font-family: inherit; text-align: center">
We are very excited to see your creations with these new presets and are open for any suggestions of what other type of presets you would like.
</div><div></div></div></td>
      </tr>
    </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" class="module" data-role="module-button" data-type="button" role="module" style="table-layout:fixed" width="100%" data-muid="4ywPd9vJ6WFyV1Si75h9vh"><tbody><tr><td align="center" bgcolor="#2e2e2e" class="outer-td" style="padding:10px 10px 60px 10px; background-color:#2e2e2e;"><table border="0" cellpadding="0" cellspacing="0" class="button-css__deep-table___2OZyb wrapper-mobile" style="text-align:center">
    <tbody>
    <tr>
    <td align="center" bgcolor="#ffffff" class="inner-td" style="border-radius:6px; font-size:16px; text-align:center; background-color:inherit;">
    ${data.map(item => `<div class="purchaseditems">
    <h4>${item.name}</h4>
    Mobile format: <a href=${item.signedUrls.mobileUrl} >here</a><br />
    Desktop format: <a href=${item.signedUrls.desktopUrl}>here</a>
  </div>`
  )}
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
    <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="6jxKSRk9dKQ1Tvi1wtnu8q">
      <tbody>
      <tr>
        <td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
          <img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:'Roboto', sans-serif; font-size:16px; max-width:100% !important; width:100%; height:auto !important;" src="https://res.cloudinary.com/angeloron/image/upload/q_100/v1581448319/loronpresets/banner.jpg" alt="" width="600" data-responsive="true" data-proportionally-constrained="false">
        </td>
      </tr>
    </tbody>
    </table>
    <table class="module" role="module" data-type="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="mVyZz43HETwfwb72TGh4iy">
      <tbody><tr>
        <td style="padding:0px 0px 0px 0px;" role="module-content" height="100%" valign="top" bgcolor="">
          <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="3px" style="line-height:3px; font-size:3px;">
            <tbody><tr>
              <td style="padding:0px 0px 3px 0px;" bgcolor="#ffffff"></td>
            </tr>
          </tbody></table>
        </td>
      </tr>
    </tbody></table><table class="module" role="module" data-type="spacer" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="sfek66tVLi5d2iy5jmSawj">
      <tbody><tr>
        <td style="padding:0px 0px 30px 0px;" role="module-content" bgcolor="">
        </td>
      </tr>
    </tbody></table>
          </td>
        </tr>
      </tbody>
    </table>
   </td>
                                      </tr>
                                    </tbody></table>
                                    <!--[if mso]>
                                  </td>
                                </tr>
                              </table>
                            </center>
                            <![endif]-->
                          </td>
                        </tr>
                      </tbody></table>
                    </td>
                  </tr>
                </tbody>
                </table>
        </div>
      </center>
    
  
</body></html>`,
  text: (data, transactionid) => `LORON PRESETS
    
    Thank you for your purchase!
    
    You can download your new presets on the links below and immediately start using them.
    
    These links are only available for 24hrs, so be fast and if you were to delete the files by mistake, no worry, you can always contact us via loronpresets.com ( //loronpresets.com ) and provide us with your email address and purchase number (${transactionid}) to get a new link.
    
    We are very excited to see your creations with these new presets and are open for any suggestions of what other type of presets you would like.
 
    ${data.map(item => `${item.name}
    Mobile format: ${item.signedUrls.mobileUrl}
    Desktop format: ${item.signedUrls.desktopUrl}`
  )}`
}